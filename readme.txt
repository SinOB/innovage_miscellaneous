=== Innovage_Miscellaneous ===
Contributors: SinOB
Requires at least: 4.0.1
Tested up to: 4.0.1
Stable tag: 0.1
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Miscellaneous functions to support iStep that do not fit into the other plugins
or the theme.

== Description ==

Miscellaneous functions to support iStep that do not fit into the other plugins
or the theme. Functions relate to the following;
* Table installation for handing historical deleted groups and users
* A new version for the user notification email
* Delete user - throw data in a table
* Delete group - throw dara in a table
* Show buddypress name in column on admin dashboard -> users page -> list of users


== Installation ==

1. Download
2. Upload to your '/wp-contents/plugins/' directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.
