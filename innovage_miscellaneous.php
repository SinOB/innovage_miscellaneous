<?php

/**
 * Plugin Name: Innovage Miscellaneous
 * Plugin URI: https://bitbucket.org/SinOB/innovage_miscellaneous
 * Description: Miscellaneous functions to support istep that do not fit into 
 * the other plugins or the theme.
 * Version: 0.1
 * Author: Sinead O'Brien
 * Author URI: https://bitbucket.org/SinOB
 * Requires at least: 4.0.1
 * Tested up to: 4.0.1
 * License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/gpl.html
 */

global $innovage_miscellaneous_db_version;
$innovage_miscellaneous_db_version = "1.0";

register_activation_hook(__FILE__, 'innovage_miscellaneous_install');

/** /
 * Create the database table when the plugin is installed.
 * Also create a db version so can perform table updates later if necessary.
 * @global type $wpdb
 * @global string $innovage_pedometer_db_version
 */
function innovage_miscellaneous_install() {
    global $wpdb;
    global $innovage_miscellaneous_db_version;

    $table_name1 = $wpdb->prefix . "bp_groups_deleted";
    $table_name2 = $wpdb->prefix . "bp_groups_members_deleted";
    $table_name3 = $wpdb->prefix . "users_deleted";

    if (get_option("innovage_miscellaneous_db_version") != $innovage_miscellaneous_db_version) {
        $sql1 = "CREATE TABLE $table_name1 (
            id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            creator_id bigint(20) NOT NULL,
            name varchar(100) NOT NULL,
            slug varchar(200) NOT NULL,
            description longtext NOT NULL,
            status varchar(10) NOT NULL DEFAULT 'public',
            challenge_type bigint(20) unsigned NOT NULL,
            challenge_end_date datetime NOT NULL,
            date_created datetime NOT NULL,
            deleted_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            deleted_by bigint(20) unsigned NOT NULL,
            KEY creator_id (creator_id),
            KEY status (status)
            );";

        $sql2 = "CREATE TABLE $table_name2 (
            id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            group_id bigint(20) NOT NULL,
            user_id bigint(20) NOT NULL,
            is_admin tinyint(1) NOT NULL DEFAULT '0',
            is_mod tinyint(1) NOT NULL DEFAULT '0',
            date_modified datetime NOT NULL,
            deleted_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            deleted_by bigint(20) unsigned NOT NULL,
            KEY group_id (group_id),
            KEY user_id (user_id)
             );";

        $sql3 = "CREATE TABLE $table_name3 (
            id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            user_id bigint(20) NOT NULL,
            user_login varchar(60) NOT NULL,
            email  varchar(100) NOT NULL,
            user_bpname varchar(100) NOT NULL,
            gender varchar(60) NOT NULL,
            year_of_birth int(11) not null default 0,
            user_registered datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            user_status int(11) not null default 0,
            deleted_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            deleted_by bigint(20) unsigned NOT NULL
              );";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql1);
        dbDelta($sql2);
        dbDelta($sql3);
    }
    update_option("innovage_miscellaneous_db_version", $innovage_miscellaneous_db_version);
}

if (!function_exists('wp_new_user_notification')) {

    /** /
     * Custom function for wp_new_user_notification - only difference should be that 
     * the user date of birth now shows up in the registration email
     *
     * @param type $user_id
     * @param type $plaintext_pass
     * @return type
     */
    function wp_new_user_notification($user_id, $plaintext_pass = '') {
        $user = get_userdata($user_id);
        // The blogname option is escaped with esc_html on the way into the database in sanitize_option
        // we want to reverse this for the plain text arena of emails.
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

        $year_of_birth = bp_get_profile_field_data(array(
            'field' => 'Year of birth',
            'user_id' => $user->ID
        ));

        $message = sprintf(__('New user registration on your site %s:'), $blogname) . "\r\n\r\n";
        $message .= sprintf(__('Username: %s'), $user->user_login) . "\r\n\r\n";
        $message .= sprintf(__('E-mail: %s'), $user->user_email) . "\r\n";
        $message .= sprintf(__('Date of birth: %s'), $year_of_birth) . "\r\n";

        @wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), $blogname), $message);

        if (empty($plaintext_pass)) {
            return;
        }

        $message = sprintf(__('Username: %s'), $user->user_login) . "\r\n";
        $message .= sprintf(__('Password: %s'), $plaintext_pass) . "\r\n";
        $message .= wp_login_url() . "\r\n";

        wp_mail($user->user_email, sprintf(__('[%s] Your username and password'), $blogname), $message);
    }

}

add_action('delete_user', 'innovage_miscellaneous_delete_user');

/** /
 * Store the user info to the historical table before delete
 * @global type $wpdb
 * @param type $user_id
 */
function innovage_miscellaneous_delete_user($user_id) {
    global $wpdb;
    $current_user_id = get_current_user_id();
    $table_name = $wpdb->prefix . "users_deleted";

    $yob = bp_get_profile_field_data(array(
        'field' => 'Year of birth',
        'user_id' => $user_id
    ));
    $gender = bp_get_profile_field_data(array(
        'field' => 'Gender',
        'user_id' => $user_id
    ));
    $user_info = get_userdata($user_id);

    $wpdb->insert($table_name, array(
        'user_id' => $user_id,
        'user_login' => $user_info->user_login,
        'email' => $user_info->user_email,
        'user_bpname' => bp_core_get_username($user_id),
        'gender' => $gender,
        'year_of_birth' => $yob,
        'user_registered' => $user_info->user_registered,
        'user_status' => $user_info->status,
        'deleted_date' => current_time('mysql', true),
        'deleted_by' => $current_user_id));
}

add_action('groups_before_delete_group', 'innovage_miscellaneous_delete_group');

/** /
 * Called before the group is deleted. Save relavent historical data to the database
 *
 * @global type $wpdb
 * @param type $group_id
 */
function innovage_miscellaneous_delete_group($group_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . "bp_groups_deleted";
    $current_user_id = get_current_user_id();
    $group = groups_get_group(array('group_id' => $group_id, 'populate_extras' => true));
    $challenge_type = groups_get_groupmeta($group_id, 'challenge-type');
    $challenge_end_date = groups_get_groupmeta($group_id, 'challenge-end-date');

    // Save the basic group info to the db
    $wpdb->insert($table_name, array('id' => $group_id,
        'creator_id' => $group->creator_id,
        'name' => $group->name,
        'slug' => $group->slug,
        'description' => $group->description,
        'status' => $group->status,
        'challenge_type' => $challenge_type,
        'challenge_end_date' => $challenge_end_date,
        'date_created' => $group->date_created,
        'deleted_date' => current_time('mysql', true),
        'deleted_by' => $current_user_id));

    if (bp_group_has_members("group_id=" . $group_id . '&exclude_admins_mods=0')) {
        while (bp_group_members()) : bp_group_the_member();
            $member_id = bp_get_group_member_id();
            // Store historical group member info to database
            innovage_miscellaneous_delete_group_member($group_id, $member_id);
        endwhile;
    }
}

// for all cases where a member leaves a group - track their membership
add_action('groups_remove_member', 'innovage_miscellaneous_delete_group_member', 15, 2);
add_action('groups_leave_group', 'innovage_miscellaneous_delete_group_member', 10, 2);

/** /
 * Store historical group membership info to the database
 *
 * @param type $group_id
 * @param type $user_id
 */
function innovage_miscellaneous_delete_group_member($group_id, $user_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . "bp_groups_members_deleted";
    $current_user_id = get_current_user_id();
    $group = groups_get_group(array('group_id' => $group_id));

    // Save the historical group members to the database
    $is_admin = BP_Groups_Member::check_is_admin($user_id, $group_id);
    $is_mod = BP_Groups_Member::check_is_mod($user_id, $group_id);

    $wpdb->insert($table_name, array(
        'group_id' => $group_id,
        'user_id' => $user_id,
        'is_admin' => $is_admin,
        'is_mod' => $is_mod,
        'date_modified' => $group->date_created,
        'deleted_date' => current_time('mysql', true),
        'deleted_by' => $current_user_id));
}

/** /
 * Add buddypress name column to admin users list 
 * @param array $column
 * @return string
 */
function innovage_miscellaneous_admin_user_table($column) {

    // Display the buddypress extended name 
    $column['Buddypress Name'] = 'Buddypress Name';

    // Hide the wordpress name
    unset($column['name']);

    return $column;
}

add_filter('manage_users_columns', 'innovage_miscellaneous_admin_user_table');

/** /
 * Set column Buddypress Name to output the buddypress extended name
 * @param type $val
 * @param type $column_name
 * @param type $user_id
 * @return type
 */
function innovage_miscellaneous_modify_user_table_row($val, $column_name, $user_id) {
    switch ($column_name) {
        case 'Buddypress Name' :
            $return = bp_core_get_user_displayname($user_id);
            break;
        default:
    }

    return $return;
}

add_filter('manage_users_custom_column', 'innovage_miscellaneous_modify_user_table_row', 10, 3);

